FROM nginx:stable
COPY ./docker/default.conf /etc/nginx/conf.d/
RUN mkdir -p /www
RUN chown -R nginx:nginx /www
RUN chmod -R 755 /www
VOLUME /www
EXPOSE 80
EXPOSE 443

