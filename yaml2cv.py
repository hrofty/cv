# Hrofty's yaml resume builder
# hrofty.net

import yaml
import qrcode
from weasyprint import HTML, CSS
from airium import Airium

# Loading yaml file
with open('cv.yaml') as cvfile:
    cvdata = yaml.safe_load(cvfile)
#print(cvdata)    
print("yaml loaded")

# Generating QR Code
qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_L,
    box_size=3,
    border=1)
qr.add_data(cvdata["header"]['qr'])
qr.make(fit=True)
img = qr.make_image(fill_color="black", back_color="white")
img.save("www/cvqr.png")
print("qr code generated sucsessfuly")

# Generating html page using Airium library
htmla = Airium()
htmla('<!DOCTYPE html>')
with htmla.html(lang="en-GB"):
    with htmla.head():
        htmla.meta(charset="utf-8")
        htmla.title(_t=cvdata["header"]['name'])
        htmla.link('rel="stylesheet" href="style.css"')
    with htmla.body():
        # Building CV header table
        with htmla.table(klass='header_table'):
            with htmla.tr():
                htmla.td(klass='cv_name', _t=cvdata["header"]['name'])
                htmla.td(_t=cvdata["header"]['phone'])
                with htmla.td('rowspan="2"'):
                    htmla.img(src='cvqr.png')
            with htmla.tr():
                htmla.td(klass='cv_jobs', _t="<br>".join(cvdata["header"]['jobs']))
                htmla.td(_t=str(cvdata["header"]['email']) + "<br>" +str(cvdata["header"]['link']))
                htmla.td()
        #MAIN BODY
        with htmla.table(klass='main_table'):
            with htmla.tr(): #Building Skills list
                htmla.td(klass='row_name').strong(_t="<hr>Skills")
                with htmla.td():                    
                    htmla.hr()
                    for skill in cvdata["skills"]:
                        htmla.p(_t="<b>" + skill + "</b>")
                        htmla.p(klass='list', _t=", ".join(cvdata["skills"][skill]))
                    
            with htmla.tr(): #Building experiences list
                htmla.td(klass='row_name').strong(_t="<hr>Experiences")
                with htmla.td():
                    htmla.hr()
                    for job in cvdata["experience"]:
                        htmla.p(_t="<b>" + job + "</b> / " + str(cvdata["experience"][job]["title"]))
                        htmla.p(klass='dates', _t="<i>" + str(cvdata["experience"][job]["dates"]) + "</i> " + str(cvdata["experience"][job]["location"]))
                        htmla.p(klass='list', _t=", ".join(cvdata["experience"][job]["experienses"]))
                
            with htmla.tr(): #Building certificates list
                htmla.td(klass='row_name').strong(_t="<hr>Certificates")
                with htmla.td():
                    htmla.hr()                
                    for system in cvdata["certificates"]:
                        htmla.b(_t=system)
                        htmla.p(klass='list', _t=", ".join(cvdata["certificates"][system]))
                
                
            with htmla.tr(): #Building languages list
                htmla.td(klass='row_name').strong(_t="<hr>Languages")
                with htmla.td():
                    htmla.hr()
                    for language in cvdata["languages"]:
                         htmla.p(_t="<b>" + language + "</b> - " + str(cvdata["languages"][language]))
                
                
            with htmla.tr(): #Building education lis
                htmla.td(klass='row_name').strong(_t="<hr>Education")
                with htmla.td():
                    htmla.hr()
                    for place in cvdata["education"]:
                        htmla.p(_t="<b>" + place + "</b> / " + str(cvdata["education"][place]["division"]))
                        htmla.p(klass='dates', _t="<i>" + str(cvdata["education"][place]["dates"]) + "</i> " + str(cvdata["education"][place]["degree"]))
print("webpage populated")                                        
# Saving HTML page            
htmlstr = str(htmla)
htmlfile = open("www/index.html", "w")
htmlfile.write(htmlstr)
htmlfile.close()
print("webpage saved")

#Generating PDF
pdfhtml = HTML(string=htmlstr, base_url='www/')
pdfcss = CSS(filename='www/style.css')
pdfhtml.write_pdf('www/cv.pdf', stylesheets=[pdfcss])
print("pdf file generated")

#All done
